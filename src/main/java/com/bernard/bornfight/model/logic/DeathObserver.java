package com.bernard.bornfight.model.logic;

public interface DeathObserver {
  // passes object that should be considered "dead"
  void onDeath(Damageable damageable);
}

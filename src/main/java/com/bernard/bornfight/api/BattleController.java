package com.bernard.bornfight.api;

import com.bernard.bornfight.model.entity.Archer;
import com.bernard.bornfight.model.entity.Army;
import com.bernard.bornfight.model.entity.Knight;
import com.bernard.bornfight.model.entity.Mage;
import com.bernard.bornfight.model.logic.BattleSimulatorFactory;
import com.bernard.bornfight.model.logic.StrategyFactory;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@Log
public class BattleController {

  // userId -> belonging army
  private Map<Integer, Army> armyRepo = new HashMap<>();
  // user <-> user who are fighting
  private Map<Integer, Integer> battleRepo = new HashMap<>();
  // temporary pool for challenges between players
  private Map<Integer, List<Integer>> challengeRepo = new HashMap<>();

  @GetMapping("/register/{id}")
  public String register(@PathVariable Integer id) {
    if (armyRepo.get(id) != null)
      return "User already exists!";
    armyRepo.put(id, new Army(new ArrayList<>() {{
      // using 10 warriors of each class as default
      IntStream.range(0, 10).forEach(i -> {
        add(new Archer());
        add(new Knight());
        add(new Mage());
      });
    }}));
    return "Your army was created (10, 10, 10)";
  }

  @GetMapping("/challenge")
  public String challenge(@RequestParam("id") int id, @RequestParam("opponentId") int opponentId) {
    if (!challengeRepo.containsKey(id))
      challengeRepo.put(id, new ArrayList<>());
    challengeRepo.get(id).add(opponentId);
    log.info(String.format("Player %s challenged %s", id, opponentId));
    return "You have created a challenge!";
  }

  @GetMapping("/status")
  public String status(@RequestParam("id") int id) {
    if (!armyRepo.containsKey(id))
      return "User doesn't exist";
    var a = armyRepo.get(id);
    log.info(String.format("User %s read status", id));
    return
      String.format("Your army status:\n" + a.getAllWarriors()
        .stream()
        .map(w -> w.getClassName() + String.format(" (%s HP)", w.getHp()))
        .collect(Collectors.joining("\n")));
  }

  /**
   * Automatski dependency injection koji nudi spring boot sa anotacijama
   * namjerno nisam koristio zato što sam, ('for the sake of this task'
   * koji provjerava znanje arhitekturnih patterna) iskoristiti factory
   * pattern koji je zapravo isto što i dependency injection.
   *
   * Praktično, svaki arhitekturni obrazac u svojoj srži manifestira
   * inverziju kontrole u nekom obliku.
   *
   * @param id
   * @param opponentId
   * @param strategy
   * @return
   */
  @GetMapping("/attack")
  public String attack(@RequestParam("id") int id, @RequestParam("opponentId") int opponentId, @RequestParam("strat") String strategy) {
    if (opponentId != battleRepo.get(id)) {
      return "No such battle!";
    }
    log.info(String.format("Performing battle between %s and %s", id, opponentId));
    var user = armyRepo.get(id);
    var opponent = armyRepo.get(opponentId);
    var strat = StrategyFactory.getByName(strategy); // getting strategy which was requested
    var wave = strat.applyTo(user); // taking portion of army, it removes those troops from actual army
    var simulator = BattleSimulatorFactory.getRealisticBattleSimulator();
    var result = simulator.simulate(wave, opponent);
    // after the attack, troops return to their army
    wave.getAllWarriors().forEach(user::addWarrior);
    if (user.getAllWarriors().size() == 0) {
      armyRepo.remove(id);
      return result + "You lost!\n";
    }
    if (opponent.getAllWarriors().size() == 0) {
      armyRepo.remove(opponentId);
      return result + "You won!\n";
    }
    return result + "The attack was carried out!\n" + status(id);
  }

  @GetMapping("/accept")
  public String accept(@RequestParam("id") int id, @RequestParam("opponentId") int opponentId) {
    if (!challengeRepo.containsKey(opponentId))
      return "User doesn't exist";
    if (challengeRepo.get(opponentId).contains(id))
      challengeRepo.get(opponentId).remove((Integer) id);
    else
      return "No challenge for user!";
    log.info(String.format("Player %s accepted %s's challenge", id, opponentId));
    battleRepo.put(id, opponentId);
    battleRepo.put(opponentId, id);
    return "Battle started!";
  }

  @GetMapping("/reject")
  public String reject(@RequestParam("id") int id, @RequestParam("opponentId") int opponentId) {
    challengeRepo.getOrDefault(opponentId, new ArrayList<>()).remove((Integer) id);
    return null;
  }

  @GetMapping("/list")
  public String listChallenges(@RequestParam("id") int id) {
    return challengeRepo.getOrDefault(id, List.of())
      .stream()
      .map(String::valueOf)
      .collect(Collectors.joining("\n")) + "\n";
  }
}

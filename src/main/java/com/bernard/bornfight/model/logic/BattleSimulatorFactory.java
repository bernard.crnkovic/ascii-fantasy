package com.bernard.bornfight.model.logic;

import java.util.Random;

public class BattleSimulatorFactory {

  // for testing purposes
  public static BattleSimulator getInstantWinBattleSimulator() {
    return (attacker, defender) -> {
      // battle logic
      // everyone from opponents army dies immediately
      defender.getAllWarriors().forEach(w -> w.inflict(1000));
      return "You enemy didn't stand a chance with your godlike powers"; // winner
    };
  }

  // attempt at implementing fair battle simulation
  public static BattleSimulator getRealisticBattleSimulator() {
    return ((attacker, defender) -> {
      var attackerWarriors = attacker.getAllWarriors();
      var defenderWarriors = defender.getAllWarriors();
      // 1/5 chance that angry wolves attack
      if (new Random().nextInt(5) == 0) {
        var deadMan = attackerWarriors.get(new Random().nextInt(attackerWarriors.size()));
        deadMan.inflict(1000);
        return "A pack of bloodthirsty wolves attacked your troops. Due to circumstances, battle was aborted.\n" +
          "Casualties: ✝ 1 " + deadMan.getClassName() + "\n";
      }
      // every attacker deals 0-100 damage (random) to some defender
      attackerWarriors.forEach(a -> defenderWarriors.get(new Random().nextInt(defenderWarriors.size())).inflict(new Random().nextInt(100)));
      // during defense, attacker also gets damaged (0-60) by some defender
      defenderWarriors.forEach(d -> attackerWarriors.get(new Random().nextInt(attackerWarriors.size())).inflict(new Random().nextInt(60)));
      if (defenderWarriors.isEmpty())
        return "Attacker caught an army off guard and annihilated it!\n";
      if (attackerWarriors.isEmpty())
        return "Seems like defenders were ready for this one! All of attacker's troops died.\n";
      // both armies survived to some degree
      return "";
    });
  }
}

package com.bernard.bornfight.model.entity;

public class Archer extends AbstractWarrior {

  public Archer() {
    super(100, 300);
  }

  @Override
  public String getDesctription() {
    return "Stealth and large attack range are one of many upsides of Archers.";
  }

  @Override
  public String getClassName() {
    return "Archer";
  }
}

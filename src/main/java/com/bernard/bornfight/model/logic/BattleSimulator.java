package com.bernard.bornfight.model.logic;

import com.bernard.bornfight.model.entity.Army;

public interface BattleSimulator {
  String simulate(Army attacker, Army defender);
}

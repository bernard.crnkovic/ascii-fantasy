package com.bernard.bornfight.model.entity;

public class Mage extends AbstractWarrior {

  public Mage() {
    super(200, 200);
  }

  @Override
  public String getClassName() {
    return "Mage";
  }

  @Override
  public String getDesctription() {
    return
      "Mages are misterious interdimensional travellers\n" +
      "with powerful dark spells that shatter their enemies\n" +
      "using only their staff & book of wisdom. However, they are not very agile.";
  }
}

package com.bernard.bornfight.model.logic;

import com.bernard.bornfight.model.entity.Wave;
import com.bernard.bornfight.model.entity.Army;

/**
 * Specification of battle strategy implemented using strategy pattern
 * (pun intended)
 */
public interface Strategy {
  Wave applyTo(Army army);
}

package com.bernard.bornfight.model.logic;

import java.util.HashMap;
import java.util.Map;

// A factory that implements a execution plan of attack
public class StrategyFactory {

  private static Map<String, Strategy> strats = new HashMap<>() {{
    put("default", getDefaultStrategy());
    put("aggressive", getAggressiveStrategy());
  }};

  public static Strategy getByName(String name) {
    var strat = strats.get(name);
    if (strat == null)
      throw new UnsupportedOperationException(String.format("Strategy %s doesn't exist", name));
    return strat;
  }

  public static Strategy getDefaultStrategy() {
    return (a) -> {
      // More versatile army
      // Will it help an army win or seal its damnation?
      // Find out by playing
      return a.getNextWave(new HashMap<>() {{
        put("archer", 4);
        put("mage", 3);
        put("knight", 3);
      }});
    };
  }

  public static Strategy getAggressiveStrategy() {
    return (a) -> {
      // Build aggressive strategy
      // For example, use only knights
      // because they are merciless
      return a.getNextWave(new HashMap<>() {{
        put("archer", 0);
        put("mage", 0);
        put("knight", 10);
      }});
    };
  }
}

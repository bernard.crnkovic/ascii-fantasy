# Fantasy text-based igrica

- Rješio sam zadatak u Spring bootu kako sam bio najavio
- Za rješenje mi je bilo potrebno 1.5 sata
- Ukratko, primijenio sam nekoliko basic design patterna: nasljeđivanje, observer pattern, factory za
decoupling konstrukcije objekata od logike, strategy pattern za stvaranje strategije bitaka
- Logiku sam više manje izveo u samom kontroleru jer je bila prilično jednostavna
- Složena business logika bi se konvencionalno u springu odvojila u paket “service”
- Springov maven plugin ima task spring-boot:run koji bi trebao pokrenuti tomcat na portu :8080

```bash
./mvnw clean spring-boot:run
```



Primjer korištenja igrice nalazi se u example.sh
```bash
# registering players
curl "localhost:8080/register/1"
curl "localhost:8080/register/2"
# challenging 1 -> 2
curl "localhost:8080/challenge?id=1&opponentId=2"
# accepting challenge
curl "localhost:8080/accept?id=2&opponentId=1"
# attacking 1 -> 2
curl "localhost:8080/attack?id=1&opponentId=2&strat=default"
# attacking 2 -> 1
curl "localhost:8080/attack?id=2&opponentId=1&strat=default"
```

Za debug logging sam na par mjesta iskoristio Lombok pretprocessor koji injektira loggera pri prevođenju programa.

*Napomena: S obzirom na to da je naglasak zadatka na praksama programiranja i interakciji između objekata, izostavio sam perzistenciju i autentifikaciju & all the good stuff koji spring inače nudi. Sve ide preko GET upita tako da se kontekst subjekta (korisnik) oslovljava sa id=… query parametrom*

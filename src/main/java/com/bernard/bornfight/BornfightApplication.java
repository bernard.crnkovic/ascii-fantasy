package com.bernard.bornfight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BornfightApplication {

	// for this simple application, repository and services
	// were not necessary, everything was implemented using
	// observer pattern in models
	public static void main(String[] args) {
		SpringApplication.run(BornfightApplication.class, args);
	}

}

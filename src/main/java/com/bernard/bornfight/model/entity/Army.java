package com.bernard.bornfight.model.entity;

import com.bernard.bornfight.model.logic.Damageable;
import com.bernard.bornfight.model.logic.DeathObserver;
import lombok.extern.java.Log;

import java.util.*;

@Log
public class Army implements DeathObserver {

  private Set<AbstractWarrior> warriors = new HashSet<>();

  public Army(List<AbstractWarrior> warriors) {
    warriors.forEach(this::addWarrior);
  }

  /**
   * @param plan Map containing amount of warriors to extract from army
   */
  public Wave getNextWave(Map<String, Integer> plan) {
    Wave wave = new Wave(new ArrayList<>());
    warriors.stream()
      .filter(w -> w instanceof Archer)
      .limit(plan.getOrDefault("archer", 0))
      .forEach(wave::addWarrior);
    warriors.stream()
      .filter(w -> w instanceof Knight)
      .limit(plan.getOrDefault("knight", 0))
      .forEach(wave::addWarrior);
    warriors.stream()
      .filter(w -> w instanceof Mage)
      .limit(plan.getOrDefault("mage", 0))
      .forEach(wave::addWarrior);
    warriors.removeAll(wave.getAllWarriors());
    return wave;
  }

  public List<AbstractWarrior> getAllWarriors() {
    return new ArrayList<>(warriors);
  }

  public void addWarrior(AbstractWarrior warrior) {
    warrior.addDeathObserver(this); // attaching listener in case warrior dies
    warriors.add(warrior);
  }

  public int getArcherCount() {
    return (int) warriors.stream().filter(w -> w instanceof Archer).count();
  }

  public int getMageCount() {
    return (int) warriors.stream().filter(w -> w instanceof Mage).count();
  }

  public int getKnightCount() {
    return (int) warriors.stream().filter(w -> w instanceof Knight).count();
  }

  @Override
  public void onDeath(Damageable damageable) {
    log.info("RIP ✝ " + damageable.getClass().getSimpleName());
    warriors.remove(damageable);
  }
}

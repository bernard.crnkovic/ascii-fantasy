package com.bernard.bornfight.model.entity;

import com.bernard.bornfight.model.entity.AbstractWarrior;
import com.bernard.bornfight.model.entity.Army;

import java.util.List;

/**
 * A portion of army,
 * it also acts as an army of its own,
 * so we might as well inherit those props
 * from parent
 */
public class Wave extends Army {
  public Wave(List<AbstractWarrior> warriors) {
    super(warriors);
  }
}

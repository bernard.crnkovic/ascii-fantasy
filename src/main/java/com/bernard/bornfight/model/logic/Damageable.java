package com.bernard.bornfight.model.logic;

public interface Damageable {
  // inflicts {hp} points of damage to entity
  void inflict(int hp);
  // subscribes
  void addDeathObserver(DeathObserver observer);

}

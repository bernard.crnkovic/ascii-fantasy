package com.bernard.bornfight.model.entity;

public class Knight extends AbstractWarrior {

  public Knight() {
    super(300, 100);
  }

  @Override
  public String getClassName() {
    return "Knight";
  }

  @Override
  public String getDesctription() {
    return
      "Knights are known for their ruthlessness & thirst for blood.\n" +
      "Their rage is meaningless at long distances though.";
  }

}

package com.bernard.bornfight.model.entity;

import com.bernard.bornfight.model.logic.Damageable;
import com.bernard.bornfight.model.logic.DeathObserver;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractWarrior implements Damageable {

  private int hp;
  private int energy;
  // in this implementation only army object
  // is a death observer because on death event
  // it removes warrior from collection
  // but we could also have multiple unrelated observers that do something in case of warriors death
  // this is just to show usage of listener pattern
  private List<DeathObserver> deathObservers = new ArrayList<>();

  public AbstractWarrior(int hp, int energy) {
    this.hp = hp;
    this.energy = energy;
  }

  public abstract String getClassName();

  public abstract String getDesctription();

  @Override
  public void inflict(int hp) {
    this.hp -= hp;
    if (this.hp < 0)
      deathObservers.forEach(o -> o.onDeath(this));
  }

  @Override
  public void addDeathObserver(DeathObserver observer) {
    deathObservers.add(observer);
  }

  public int getHp() {
    return hp;
  }
}

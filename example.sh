# registering players
curl "localhost:8080/register/1"
curl "localhost:8080/register/2"
# challenging 1 -> 2
curl "localhost:8080/challenge?id=1&opponentId=2"
# accepting challenge
curl "localhost:8080/accept?id=2&opponentId=1"
# attacking 1 -> 2
curl "localhost:8080/attack?id=1&opponentId=2&strat=default"
# attacking 2 -> 1
curl "localhost:8080/attack?id=2&opponentId=1&strat=default"
